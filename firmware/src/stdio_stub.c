#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>

#include "stdio_stub.h"
#include "macro.h"
#include "config.h"

#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>

#undef errno
extern int errno;

#ifdef stdin_uart
#define STDIN_UART _NTH0(stdin_uart)
#define STDIN_UART_DEV _CAT2(USART, _NTH0(stdin_uart))
#define STDIN_UART_RCC _CAT2(RCC_USART, _NTH0(stdin_uart))
#define STDIN_UART_PORT_TX _CAT4(GPIO_BANK_USART, _NTH0(stdin_uart), _NTH1(stdin_uart), _TX)
#define STDIN_UART_PORT_RX _CAT4(GPIO_BANK_USART, _NTH0(stdin_uart), _NTH1(stdin_uart), _RX)
#define STDIN_UART_PAD_TX _CAT4(GPIO_USART, _NTH0(stdin_uart), _NTH1(stdin_uart), _TX)
#define STDIN_UART_PAD_RX _CAT4(GPIO_USART, _NTH0(stdin_uart), _NTH1(stdin_uart), _RX)
#define STDIN_UART_RATE _NTH2(stdin_uart)
#endif

#ifdef stdout_uart
#define STDOUT_UART _NTH0(stdout_uart)
#define STDOUT_UART_DEV _CAT2(USART, _NTH0(stdout_uart))
#define STDOUT_UART_RCC _CAT2(RCC_USART, _NTH0(stdout_uart))
#define STDOUT_UART_PORT_TX _CAT4(GPIO_BANK_USART, _NTH0(stdout_uart), _NTH1(stdin_uart), _TX)
#define STDOUT_UART_PORT_RX _CAT4(GPIO_BANK_USART, _NTH0(stdout_uart), _NTH1(stdin_uart), _RX)
#define STDOUT_UART_PAD_TX _CAT4(GPIO_USART, _NTH0(stdout_uart), _NTH1(stdin_uart), _TX)
#define STDOUT_UART_PAD_RX _CAT4(GPIO_USART, _NTH0(stdout_uart), _NTH1(stdin_uart), _RX)
#define STDOUT_UART_RATE _NTH2(stdout_uart)
#endif

#ifdef stderr_uart
#define STDERR_UART stderr_uart
#define STDERR_UART_DEV _CAT2(USART, _NTH0(stderr_uart))
#define STDERR_UART_RCC _CAT2(RCC_USART, _NTH0(stderr_uart))
#define STDERR_UART_PORT_TX _CAT4(GPIO_BANK_USART, _NTH0(stderr_uart), _NTH1(stdin_uart), _TX)
#define STDERR_UART_PORT_RX _CAT4(GPIO_BANK_USART, _NTH0(stderr_uart), _NTH1(stdin_uart), _RX)
#define STDERR_UART_PAD_TX _CAT4(GPIO_USART, _NTH0(stderr_uart), _NTH1(stdin_uart), _TX)
#define STDERR_UART_PAD_RX _CAT4(GPIO_USART, _NTH0(stderr_uart), _NTH1(stdin_uart), _RX)
#define STDERR_UART_RATE _NTH2(stderr_uart)
#endif

void stdio_init(void) {
#if defined(STDIN_UART)
  /* Enable USART clock */
  rcc_periph_clock_enable(STDIN_UART_RCC);
  
  gpio_set_mode(STDIN_UART_PORT_TX, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, STDIN_UART_PAD_TX);

  gpio_set_mode(STDIN_UART_PORT_RX, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, STDIN_UART_PAD_RX);

  /* Setup USART parameters */
  usart_set_baudrate(STDIN_UART_DEV, STDIN_UART_RATE);
  usart_set_databits(STDIN_UART_DEV, 8);
  usart_set_stopbits(STDIN_UART_DEV, USART_STOPBITS_1);
  usart_set_parity(STDIN_UART_DEV, USART_PARITY_NONE);
  usart_set_flow_control(STDIN_UART_DEV, USART_FLOWCONTROL_NONE);
  usart_set_mode(STDIN_UART_DEV, USART_MODE_TX_RX);

  /* Enable the USART */
  usart_enable(STDIN_UART_DEV);
#endif
  
#if defined(STDOUT_UART) && (!defined(STDIN_UART) || STDIN_UART != STDOUT_UART)
  /* Enable USART clock */
  rcc_periph_clock_enable(STDOUT_UART_RCC);
  
  gpio_set_mode(STDOUT_UART_PORT_TX, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, STDOUT_UART_PAD_TX);

  gpio_set_mode(STDOUT_UART_PORT_RX, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, STDOUT_UART_PAD_RX);

  /* Setup USART parameters */
  usart_set_baudrate(STDOUT_UART_DEV, STDOUT_UART_RATE);
  usart_set_databits(STDOUT_UART_DEV, 8);
  usart_set_stopbits(STDOUT_UART_DEV, USART_STOPBITS_1);
  usart_set_parity(STDOUT_UART_DEV, USART_PARITY_NONE);
  usart_set_flow_control(STDOUT_UART_DEV, USART_FLOWCONTROL_NONE);
  usart_set_mode(STDOUT_UART_DEV, USART_MODE_TX_RX);

  /* Enable the USART */
  usart_enable(STDOUT_UART_DEV);
#endif

#if defined(STDERR_UART) && (!defined(STDOUT_UART) || STDOUT_UART != STDOUT_UART) && (!defined(STDIN_UART) || STDIN_UART != STDERR_UART)
  /* Enable USART clock */
  rcc_periph_clock_enable(STDERR_UART_RCC);

  gpio_set_mode(STDERR_UART_PORT_TX, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, STDERR_UART_PAD_TX);

  gpio_set_mode(STDERR_UART_PORT_RX, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, STDERR_UART_PAD_RX);

  /* Setup USART parameters */
  usart_set_baudrate(STDERR_UART_DEV, STDERR_UART_RATE);
  usart_set_databits(STDERR_UART_DEV, 8);
  usart_set_stopbits(STDERR_UART_DEV, USART_STOPBITS_1);
  usart_set_parity(STDERR_UART_DEV, USART_PARITY_NONE);
  usart_set_flow_control(STDERR_UART_DEV, USART_FLOWCONTROL_NONE);
  usart_set_mode(STDERR_UART_DEV, USART_MODE_TX_RX);

  /* Enable the USART */
  usart_enable(STDERR_UART_DEV);
#endif
}

void stdio_done(void) {
#if defined(STDIN_UART)
  /* Disable the USART */
  usart_disable(STDIN_UART_DEV);

  /* Disable USART clock */
  rcc_periph_clock_disable(STDIN_UART_RCC);
#endif
  
#if defined(STDOUT_UART) && (!defined(STDIN_UART) || STDIN_UART != STDOUT_UART)
  /* Disable the USART */
  usart_disable(STDOUT_UART_DEV);

  /* Disable USART clock */
  rcc_periph_clock_disable(STDOUT_UART_RCC);
#endif

#if defined(STDERR_UART) && (!defined(STDOUT_UART) || STDOUT_UART != STDOUT_UART) && (!defined(STDIN_UART) || STDIN_UART != STDERR_UART)
  /* Disable the USART */
  usart_disable(STDERR_UART_DEV);

  /* Disable USART clock */
  rcc_periph_clock_disable(STDERR_UART_RCC);
#endif
}

#if 0
int _isatty(int file);
int _isatty(int file) {
  switch (file){
#ifdef STDIN_UART
  case STDIN_FILENO:
#endif
#ifdef STDOUT_UART
  case STDOUT_FILENO:
#endif
#ifdef STDERR_UART
  case STDERR_FILENO:
#endif
    return 1;
  default:
    errno = EBADF;
    return 0;
  }
}
#endif

#if defined(STDIN_UART)
int _read(int file, char *ptr, int len);
int _read(int file, char *ptr, int len) {
  switch (file) {
#ifdef STDIN_UART
  case STDIN_FILENO: {
    char *end = ptr + len;
    for (; ptr < end; )
      *ptr++ = usart_recv_blocking(STDIN_UART_DEV);
  } break;
#endif
  default:
    errno = EBADF;
    return -1;
  }
  
  return len;
}
#endif

#if defined(STDOUT_UART) || defined(STDERR_UART)
int _write(int file, char *ptr, int len);
int _write(int file, char *ptr, int len) {
  switch (file) {
#ifdef STDOUT_UART
  case STDOUT_FILENO: {
    char *end = ptr + len;
    for (; ptr < end; )
      usart_send_blocking(STDOUT_UART_DEV, *ptr++);
  } break;
#endif
#ifdef STDERR_UART
  case STDERR_FILENO: {
    char *end = ptr + len;
    for (; ptr < end; )
      usart_send_blocking(STDERR_UART_DEV, *ptr++);
  } break;
#endif
  default:
    errno = EBADF;
    return -1;
  }
  return len;
}
#endif
