#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>

#include "systick.h"
#include "macro.h"
#include "config.h"

#if systick_divider == 1
#define SYSTICK_DIVIDER STK_CSR_CLKSOURCE_AHB
#else
#define SYSTICK_DIVIDER _CAT2(STK_CSR_CLKSOURCE_AHB_DIV, systick_divider)
#endif

#define SYSTICK_PERIOD ((mcu_frequency)/(systick_divider)*(systick_interval)*1000)

void systick_init(void) {
  /* Enable systick interrupt */
	nvic_enable_irq(NVIC_SYSTICK_IRQ);
  
  /* 72MHz / 8 = 9MHz counts per second */
	systick_set_clocksource(SYSTICK_DIVIDER);
  
  /* SYSTICK_PERIOD = systick_interval * 9MHz */
	systick_set_reload(SYSTICK_PERIOD-1);
  
  /* Enable systick interrupt */
	systick_interrupt_enable();
  
  /* Enable systick counter for monitor step function */
	systick_counter_enable();
}

void systick_done(void) {
  /* Disable sys tick counter for monitor step function */
  systick_counter_disable();
  systick_interrupt_disable();
  
  /* Disable systick interrupt */
	nvic_disable_irq(NVIC_SYSTICK_IRQ);
}
