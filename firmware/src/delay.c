#include <libopencm3/cm3/dwt.h>

#include "delay.h"
#include "config.h"

void delay_init() {
  dwt_enable_cycle_counter();
}

delay_t delay_snap(void) {
  return dwt_read_cycle_counter();
}

void delay_wait(delay_t sc, delay_t us) {
  uint32_t nc = sc + us * mcu_frequency;
  
  /* overflow */
  if (nc < sc) for (; dwt_read_cycle_counter() >= sc; );
  
  for (; dwt_read_cycle_counter() < nc; );
}

void delay_us(uint32_t us) {
  delay_t cc = delay_snap();
  delay_wait(cc, us);
}
