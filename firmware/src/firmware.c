#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

#include <string.h>
#include <unistd.h>
#include <stdio.h>

//#include "memtool.h"

#include "delay.h"
#include "pcd8544.h"
#include "bitmap.h"
#include "systick.h"
#include "monitor.h"
#include "stdio_stub.h"

monitor_def();

static inline void hardware_init(void) {
  /* Enable GPIO clock */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  
  /* Enable AFIO clock */
  rcc_periph_clock_enable(RCC_AFIO);
  
  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA1);
}

static inline void hardware_done(void) {
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA1);

  /* Disable AFIO clock */
  rcc_periph_clock_disable(RCC_AFIO);

  /* Disable GPIO clock */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOB);
}

static inline void init(void) {
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  
  //mem_prefill();

  delay_init();
  hardware_init();
  //stdio_init();
  pcd8544_init();
  systick_init();
  
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO0 | GPIO1);

  /*
  gpio_set(GPIOA, GPIO7);
  gpio_clear(GPIOA, GPIO5);
  */
}

#if 0
static inline void done(void) {
  systick_done();
  pcd8544_done();
  //stdio_done();
  hardware_done();
}
#else
#define done()
#endif

static bitmap_state_t canvas_state;

static const bitmap_canvas_t canvas = {
  pcd8544_frame_buffer,
  { {{ 0, 0 }}, {{ 84, 48 }} },
  &canvas_state
};

int main(void) {
  init();

  monitor_init();

  pcd8544_backlight(1);

  bitmap_init(&canvas);

  bitmap_font_def(font_8x8);
  bitmap_font_def(font_8x16);
  bitmap_font(&canvas, &font_8x16);

  bitmap_operation(&canvas, bitmap_clear);
  bitmap_apply(&canvas);

  bitmap_operation(&canvas, bitmap_draw);
  bitmap_primitive(&canvas, bitmap_line);
  
  {
    bitmap_point_t points[] = {
      /*{{ 10, 10 }},
        {{ 30, 10 }},
        {{ 30, 30 }},
        {{ 10, 30 }},*/
      {{ 0, 0 }},
      {{ 83, 47 }},
      {{ 0, 47 }},
      {{ 83, 0 }},
      {{ 0, 0 }},
      {{ 0, 47 }},
      {{ 83, 47 }},
      {{ 83, 0 }},
    };
    //bitmap_polygon(&canvas, bitmap_draw | bitmap_line | 3, points, bitmap_numof(points));
  }

  {
    bitmap_point_t points[] = {
      {{ 0, 0 }},
      {{ 83, 0 }},
      {{ 83, 47 }},
      {{ 0, 47 }},
      {{ 0, 0 }},
    };
    //bitmap_polygon(&canvas, bitmap_invert | bitmap_line | 3, points, bitmap_numof(points));
  }
  
  //bitmap_circle(&canvas, bitmap_invert | bitmap_line, 41, 23, 20);
  //bitmap_operation(&canvas, bitmap_line | bitmap_fill);
  //bitmap_rect(&canvas, 0, 0, 83, 47);
  //bitmap_rect(&canvas, 1, 1, 5, 3);
  
  //memset(pcd8544_frame_buffer, 0xff, sizeof(pcd8544_frame_buffer));
  //pcd8544_update();

  //bitmap_align(&canvas, bitmap_top | bitmap_left);
  //bitmap_text(&canvas, 0, 0, "Hello utf?");

  bitmap_align(&canvas, bitmap_top | bitmap_bottom | bitmap_left | bitmap_right);
  int8_t i = 0, s = 1;
  
  for (;;) {
    bitmap_operation(&canvas, bitmap_clear);
    bitmap_apply(&canvas);
    bitmap_operation(&canvas, bitmap_draw);
    bitmap_text(&canvas, 41, i, "Уникод это");
    pcd8544_update();
    gpio_toggle(GPIOA, GPIO1);
    monitor_wait();
    
    if (s > 0) {
      if (i > 47 + 8)
        s = -1;
    } else {
      if (i < -8)
        s = 1;
    }

    i += s;
  }
  
  done();
  
  return 0;
}
