/* common config */
#define mcu_frequency 72
#define systick_divider 8
#define systick_interval 100

/* pcd8544 config */
#define pcd8544_spi 1,32 /* device,divider */
#define pcd8544_dma 1,3 /* controller,channel */
#define pcd8544_dc A,6 /* port,pad */
#define pcd8544_res A,3 /* port,pad */
//#define pcd8544_bl_pwm 2,3 /* timer,channel */
#define pcd8544_bl A,2 /* port,pad */

#define spi1_remap 0
#define tim2_remap 0
