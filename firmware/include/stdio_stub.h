#ifndef STDIO_STUB_H
#define STDIO_STUB_H "stdio_stub.h"

void stdio_init(void);
void stdio_done(void);

#endif /* STDIO_STUB_H */
