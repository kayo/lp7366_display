#define _CAT2_(a, b) a##b
#define _CAT2(a, b) _CAT2_(a, b)

#define _CAT3_(a, b, c) a##b##c
#define _CAT3(a, b, c) _CAT3_(a, b, c)

#define _CAT4_(a, b, c, d) a##b##c##d
#define _CAT4(a, b, c, d) _CAT4_(a, b, c, d)

#define _CAT5_(a, b, c, d, e) a##b##c##d##e
#define _CAT5(a, b, c, d, e) _CAT5_(a, b, c, d, e)

#define _NTH0_(a, ...) a
#define _NTH0(...) _NTH0_(__VA_ARGS__)

#define _NTH1_(a, b, ...) b
#define _NTH1(...) _NTH1_(__VA_ARGS__)

#define _NTH2_(a, b, c, ...) c
#define _NTH2(...) _NTH2_(__VA_ARGS__)

#define _NTH3_(a, b, c, d, ...) d
#define _NTH3(...) _NTH3_(__VA_ARGS__)
