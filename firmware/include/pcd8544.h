#include <stdint.h>

#ifndef PCD8544_H
#define PCD8544_H "pcd8544.h"

#define pcd8544_columns 84
#define pcd8544_rows 48
#define pcd8544_frame_pixels (pcd8544_columns * pcd8544_rows)
#define pcd8544_frame_bytes (pcd8544_frame_pixels / 8)

extern uint8_t pcd8544_frame_buffer[pcd8544_frame_bytes];

void pcd8544_init(void);
void pcd8544_done(void);

void pcd8544_update(void);
void pcd8544_backlight(uint16_t level);

#endif /* PCD8544_H */
