#ifndef DELAY_H
#define DELAY_H "delay.h"

#include <stdint.h>

typedef uint32_t delay_t;

void delay_init(void);

delay_t delay_snap(void);
void delay_wait(delay_t sc, delay_t us);

void delay_us(uint32_t us);

static inline void delay_ms(uint32_t ms) {
  delay_us(ms * 1000);
}

#endif /* DELAY_H */
